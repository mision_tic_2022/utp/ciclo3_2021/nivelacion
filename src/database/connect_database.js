const key_database = require('./key_database');
const mongoose = require('mongoose');

class ConnectDb {
    constructor() {
        mongoose.connect(key_database.db).then(() => {
            console.log("Conexión a BD");
        }).catch(error => {
            console.error(error);
        });
    }
}

module.exports = ConnectDb;