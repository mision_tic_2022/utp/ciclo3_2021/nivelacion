const usuario = require('../models/usuario');

class UsuarioController{

    constructor(){

    }

    crearUsuario(req, res){
        let { nombre, apellido, email, telefono } = req.body;
        if(nombre != null && apellido != null && email != null && telefono != null){
            usuario.create( req.body, (error, data) => {
                if(error){
                    res.status(500).send();
                }else{
                    res.status(201).json(data);
                }
            });
        }else{
            res.status(400).send();
        }
    }

}

module.exports = UsuarioController;