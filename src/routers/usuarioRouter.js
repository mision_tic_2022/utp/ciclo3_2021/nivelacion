const UsuarioController = require('../controllers/usuarioController');
const express = require('express');

class UsuarioRouter{

    constructor(){
        this.router = express.Router();   
        this.configRouters();     
    }

    configRouters(){
        const objUsuarioController = new UsuarioController();
        this.router.post('/usuario', objUsuarioController.crearUsuario);
    }

}

module.exports = UsuarioRouter;