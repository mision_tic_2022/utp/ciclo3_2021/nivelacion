//Importar dependencias
const express = require('express');
const mongoose = require('mongoose');

const UsuarioRouter = require('./routers/usuarioRouter');
const ConnectDb = require('./database/connect_database');

class Server{

    constructor(){
        const objConnectDb = new ConnectDb();
        this.app = express();
        this.config();
    }

    config(){
        this.app.set('port', process.env.PORT || 3000);
        this.app.use(express.json());
        //Crear ruta raíz del servidor
        let router = express.Router();
        router.get('/', (req, res)=>{
            res.status(200).json({"message": "All ok"});
        });
        //Añadir ruta a express
        this.app.use(router);

        const objUsuarioRouter = new UsuarioRouter();
        this.app.use(objUsuarioRouter.router);


        //poner el servidor a la escucha
        this.app.listen( this.app.get('port'), ()=>{
            console.log("Servidor corriendo por el puerto => ", this.app.get('port'));
        } );
    }

}

new Server();
