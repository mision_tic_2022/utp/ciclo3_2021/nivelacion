const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let usuarioSchema = new Schema({
    nombre: {
        type: String
    }, 
    apellido: {
        type: String
    },
    email: {
        type: String
    },
    telefono: {
        type: String
    }
}, {
    collection: "usuarios"
});

module.exports = mongoose.model("Usuario", usuarioSchema);